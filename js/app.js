$(function(){
    // Initialize library
    lowLag.init({'urlPrefix':'samples/','sm2url':'lib/sm2/swf/'});

    // Map keys
    var keys = {
        a : ['hh_open1'],
        A : ['hh_closed1'],
        s : ['crash1'],
        S : ['cowbell'],
        d : ['crash2'],
        f : ['ride1'],
        z : ['snare1'],
        x : ['tom1'],
        c : ['tom2'],
        v : ['tom3'],
        m : ['kick1']
    }; // Second pedal acts as shift key
    // Map gamepad to keys
    // var gamepad = {
    //
    // };

    // Load samples
    for(var key in keys){
        for(var i=0;i<keys[key].length;i++) {
            lowLag.load(keys[key][i] + '.wav', keys[key][i]);
        }
    }

    // test on keypress
    $(document).on('keypress', function(e){
        //alert(e.key);
        if(keys[e.key] || keys[e.key.toLowerCase()]) {
            var k = keys[e.key] ? e.key : e.key.toLowerCase();
            var idx = Math.floor(Math.random()*(keys[k].length));
            lowLag.play(keys[k][idx]);
            //$('#drum').innerHTML = $('div.drum').innerHTML + keys[e.key][index]+"<br />";
        }
        else {

        }
    });

    // test on gamepad button press

});
